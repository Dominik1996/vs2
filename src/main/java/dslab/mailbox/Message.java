package dslab.mailbox;

public class Message {
    private int id;
    private String subject;
    private String data;
    private String from;
    private String to;

    public Message(int id, String subject, String data, String from, String to){
        this.id = id;
        this.subject = subject;
        this.data = data;
        this.from = from;
        this.to = to;
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public String getFrom() {
        return from;
    }

    public String getSubject() {
        return subject;
    }

    public String getTo() {
        return to;
    }
}
