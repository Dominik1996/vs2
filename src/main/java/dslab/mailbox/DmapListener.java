package dslab.mailbox;

import dslab.util.Config;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class DmapListener extends Thread {
    private ServerSocket serverSocket;          // socket for new connections
    private MailboxServer parent;               // parent for messages
    List<String[]> userpwd = new ArrayList<String[]>();// contains users and passwords
    private ThreadPoolExecutor executor;        // contains all threads

    // flag to stop the server after shutdown command was inserted to CLI
    private final AtomicBoolean running = new AtomicBoolean(false);

    /**
     * create a new dmap listener
     * @param serverSocket
     * @param parent
     * @param domain
     * @param userConfig
     */
    public DmapListener(ServerSocket serverSocket, MailboxServer parent, String domain, String userConfig) {
        this.serverSocket = serverSocket;
        this.parent = parent;
        Config conf = new Config(userConfig);
        Set<String> users = conf.listKeys();
        for (String u:users) {
            String[] e = new String[2];
            e[0] = u;
            e[1] = conf.getString(u);
            userpwd.add(e);
        }
        executor = new ThreadPoolExecutor(0,Integer.MAX_VALUE,2L, TimeUnit.MINUTES, new SynchronousQueue<Runnable>());
    }

    /**
     * start endless method to accept new connections, can be stopped by the shutdown method
     */
    public void run() {
        running.set(true);
        while (running.get()) {
            Socket socket = null;
            try {
                // wait for Client to connect
                socket = serverSocket.accept();

                DmapHandler t = new DmapHandler(parent,socket,userpwd);
                executor.execute(t);
            } catch (SocketException e) {
                // when the socket is closed, the I/O methods of the Socket will throw a SocketException
                // almost all SocketException cases indicate that the socket was closed
                System.out.println("SocketException while handling socket: " + e.getMessage());
                break;
            } catch (IOException e) {
                // you should properly handle all other exceptions
                throw new UncheckedIOException(e);
            } finally {

            }

        }
    }

    public void shutdown(){
        running.set(false);
        for(Runnable t :executor.getQueue()){
            DmapHandler d = (DmapHandler) t;
            d.shutdown();
        }
    }
}
