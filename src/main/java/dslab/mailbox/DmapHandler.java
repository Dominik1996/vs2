package dslab.mailbox;

import dslab.util.Keys;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class DmapHandler extends Thread {
    private Socket clientSocket;
    private MailboxServer server;
    List<String[]> userpwd;
    private String user;
    private String componentId;
    private boolean startsecure = false;
    private PrivateKey privateKey;



    // flag to stop the server after shutdown command was inserted to CLI
    private final AtomicBoolean running = new AtomicBoolean(false);

    public DmapHandler(MailboxServer server,Socket clientSocket, List<String[]> userpwd){
        Security.addProvider(new BouncyCastleProvider());


        this.clientSocket = clientSocket;
        this.userpwd = userpwd;
        this.server = server;
    }

    PrintWriter writer;
    public void run() {
        try {
            running.set(true);

            // prepare the input reader for the socket
            BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            // prepare the writer for responding to clients requests
            writer = new PrintWriter(clientSocket.getOutputStream());

            System.out.println("client connection ready");

            // send DMAP ok message
            try {
                write("ok DMAP 2.0");
            } catch (Exception e) {
                e.printStackTrace();
            }

            String request;

            while (running.get()) {
                try {
                    if (login(writer, reader)) {
                        boolean loggedIn = true;
                        while (running.get() && loggedIn) {
                            if ((request = reader.readLine()) != null) {
                                request = decodeCypher(request);
                                System.out.println("Client sent the following request: " + request);
                                String[] parts = request.split("\\s");
                                switch (parts[0]) {
                                    case "startsecure":
                                        doSecureChannel(request, writer, reader);
                                        break;
                                    case "list":
                                        synchronized (server) {
                                            for (Message m : server.messages) {
                                                if (m.getTo().split("@")[0].equals(user)) {
                                                    write(m.getId() + " " + m.getFrom() + " " + m.getSubject());
                                                }
                                            }
                                        }
                                        break;
                                    case "show":
                                        try {
                                            int id = Integer.parseInt(parts[1]);
                                            if (id < 0)
                                                throw new NumberFormatException("error unknown message id ");
                                            synchronized (server) {
                                                boolean match = false;
                                                for (Message m : server.messages) {
                                                    if (m.getTo().split("@")[0].equals(user) && m.getId() == id) {
                                                        write("from " + m.getFrom());
                                                        write("to " + m.getTo());
                                                        write("subject " + m.getSubject());
                                                        write("data " + m.getData());
                                                        match = true;
                                                        break;
                                                    }
                                                }
                                                if(!match)
                                                    throw new NumberFormatException("error unknown message id ");
                                            }
                                        } catch (NumberFormatException e) {
                                            write("error unknown message id ");
                                        }
                                        break;
                                    case "delete":
                                        try {
                                            int id = Integer.parseInt(parts[1]);
                                            if (id < 0)
                                                throw new NumberFormatException("error unknown message id ");
                                            synchronized (server) {
                                                boolean match = false;
                                                for (Message m : server.messages) {
                                                    if (m.getTo().split("@")[0].equals(user) && m.getId() == id) {
                                                        server.messages.remove(m);
                                                        write("ok");
                                                        match = true;
                                                        break;
                                                    }
                                                }
                                                if(!match)
                                                    throw new NumberFormatException("error unknown message id ");
                                            }
                                        } catch (NumberFormatException e) {
                                            write("error unknown message id");
                                        }
                                        break;
                                    case "logout":
                                        loggedIn = false;
                                        write("ok");
                                        break;
                                    case "quit":
                                        write("ok bye");
                                        try {
                                            Thread.sleep(50);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        writer.close();
                                        clientSocket.close();
                                        break;
                                    default:
                                        continue;
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    break;
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (clientSocket != null && !clientSocket.isClosed()) {
                try {
                    clientSocket.close();
                } catch (IOException e) {
                    // Ignored because we cannot handle it
                }
            }

        }

    }

    String decodeCypher(String message) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, InvalidKeyException, InvalidAlgorithmParameterException {
        if(secure) return new String(cryptAES(Base64.decode(message), false));
        else return message;
    }

    void doSecureChannel(String request, PrintWriter writer, BufferedReader reader) throws Exception {
        write("ok " + server.getComponentId());
        //step 4: we got the challange from the client and we need to decrypt it with our prvate key and set up the aes encryption

        //get my private Key
        if (this.privateKey == null) {
            File f = new File(System.getProperty("user.dir") + "/keys/server/" + server.getComponentId());
            this.privateKey = Keys.readPrivateKey(f); //+this.userPropertyConfig.getString(componentId)));
        }
        if((request = reader.readLine()) != null) {
            System.out.println("encrypted message: " + request);
            byte[] decodeds = Base64.decode(request);
            byte[] message = decypherRsa(decodeds, privateKey);
            String decryptedm = new String(message);
            System.out.println("String: " + decryptedm);
            //decode the message with the initialized vektor and challenge
            //3. C (RSA): ok <client-challenge> <secret-key> <iv>
            String challange = decryptedm.split(" ")[1];
            key = new SecretKeySpec(decodeBytes(decryptedm.split(" ")[2]), "AES");
            msg_iv = decodeBytes(decryptedm.split(" ")[3]);

            write(byteToS(cryptAES(("ok " + challange).getBytes(), true)).toString());


            //that was the last message from the server so we need to proceed with our normal routine
            secure = true;
        }
    }

    boolean secure = false;
    byte[] msg_iv = null;
    SecretKeySpec key = null;
    public boolean login(PrintWriter writer, BufferedReader reader) throws Exception {
        String request;

        if((request = reader.readLine()) != null) {
            System.out.println("Request_encoded: " + request);
            request = decodeCypher(request);
            System.out.println("Request: " + request);
            String[] parts = request.split("\\s");

            if (request.equals("startsecure")) {
                doSecureChannel(request, writer, reader);
            }
            else if (request.contains("ok")) return false;
            else {
                if (parts.length != 3 && !parts[0].equals("login")) {
                    if (parts.length == 1 && parts[0].equals("quit")) {
                        write("ok bye");
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        writer.close();
                        clientSocket.close();
                        return false;
                    }
                    write("error not logged in or invalid login command");
                } else {
                    boolean userfound = false;
                    String u = parts[1];
                    String p = parts[2];
                    for (String[] valid : userpwd) {
                        if (u.equals(valid[0]) && p.equals(valid[1])) {
                            write("ok");
                            user = u;
                            return true;
                        } else if (u.equals(valid[0]) && !p.equals(valid[1])) {
                            write("error wrong password");
                            userfound = true;
                        }
                    }
                    if (!userfound)
                        write("error unknown user");
                }
            }
        }

        return false;
    }

    byte[] cryptAES(byte[] data, boolean encrypt) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        // make sure to use the right ALGORITHM for what you want to do (see text)
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        // MODE is the encryption/decryption mode
        // KEY is either a private, public or secret key
        // IV is an init vector, needed for AES
        cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, key, new IvParameterSpec(msg_iv));
        return cipher.doFinal(data);
    }

    byte[] decypherRsa(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        // make sure to use the right ALGORITHM for what you want to do (see text)
        Cipher rsaCipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding", "BC");
        // MODE is the encryption/decryption mode
        // KEY is either a private, public or secret key
        // IV is an init vector, needed for AES
        rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);
        return rsaCipher.doFinal(data);
    }

    private void write(String input) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        String encoded = "";
        if(secure) encoded = byteToS(cryptAES(input.getBytes(), true));
        if(secure) writer.println(encoded);
        else writer.println(input);
        writer.flush();
        System.out.println("server: " + input);
        System.out.println("server_encoded: " + encoded);
    }

    byte[] decodeBytes(String data) {
        return Base64.decode(data.getBytes());
    }

    String byteToS(byte[] data) {
        return new String(Base64.encode(data), StandardCharsets.UTF_8);
    }

    public void shutdown(){
        running.set(false);
    }
}
