package dslab.mailbox;

import dslab.transfer.TransferDmtpServer;
import dslab.util.Config;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class DmtpListener  extends Thread{
    private ServerSocket serverSocket;  // socket for new connections
    private MailboxServer parent;       // parent for messages
    private ThreadPoolExecutor executor;// contains all threads

    // flag to stop the server after shutdown command was inserted to CLI
    private final AtomicBoolean running = new AtomicBoolean(false);

    /**
     * create a new dmtp listener
     * @param serverSocket
     * @param parent
     * @param domain
     * @param userConfig
     */
    public DmtpListener(ServerSocket serverSocket, MailboxServer parent, String domain, String userConfig) {
        this.serverSocket = serverSocket;
        this.parent = parent;

        executor = new ThreadPoolExecutor(0,Integer.MAX_VALUE,2L, TimeUnit.MINUTES, new SynchronousQueue<Runnable>());
    }


    /**
     * endless loop to open new connections, can be stopped by the shutdown method
     */
    public void run() {

        running.set(true);
        Socket socket = null;
        while (running.get()) {
            try {
                // wait for Client to connect
                socket = serverSocket.accept();

                MailboxDmtpServer t = new MailboxDmtpServer(parent,socket);
                executor.execute(t);
            } catch (SocketException e) {
                // when the socket is closed, the I/O methods of the Socket will throw a SocketException
                // almost all SocketException cases indicate that the socket was closed
                System.out.println("SocketException while handling socket: " + e.getMessage());
                break;
            } catch (IOException e) {
                // you should properly handle all other exceptions
                throw new UncheckedIOException(e);
            } finally {

            }
        }
        try {
            socket.close();
        }catch (IOException ex){

        }
    }

    /**
     * simple method to shutdown the server gently
     */
    public void shutdown(){
        running.set(false);
        for(Runnable t :executor.getQueue()){
            DmapHandler d = (DmapHandler) t;
            d.shutdown();
        }
    }

}
