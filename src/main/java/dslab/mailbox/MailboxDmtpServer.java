package dslab.mailbox;

import dslab.util.Config;
import dslab.util.DmtpServer;

import java.net.Socket;
import java.util.List;
import java.util.Set;

public class MailboxDmtpServer extends DmtpServer {
    MailboxServer server;   // parent server
    Set<String> users;      // list with valid users on this server (domain)

    /**
     * constructor
     * @param parent
     * @param clientSocket
     */
    public MailboxDmtpServer(MailboxServer parent, Socket clientSocket) {
        super(parent, clientSocket);
        this.server = parent;
        Config conf = parent.usersConfig;
        users = conf.listKeys();
    }

    @Override
    public boolean handleMessageComplete(String from, List<String> to, String subject, String data) {
        boolean check = super.handleMessageComplete(from, to, subject, data);
        if(!check){
            return check;
        }

        // add messages to our local "storage"
        for (String address:to) {
                synchronized (server){
                    int id = server.messageCounter;
                    server.messageCounter++;
                    Message m = new Message(id,subject,data,from,address);
                    server.messages.add(m);
                }
        }
        return true;
    }

    @Override
    public boolean checkReceiver(String address) {
        if(!super.checkReceiver(address))
            return false;

        // only accept messages when the user is known
        String[] p = address.split("@");
        for (String u:users) {
            if(u.equals(p[0]))
                return true;
        }
        return false;
    }

}
