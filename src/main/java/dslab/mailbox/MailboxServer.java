package dslab.mailbox;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.nameserver.AlreadyRegisteredException;
import dslab.nameserver.INameserverRemote;
import dslab.nameserver.InvalidDomainException;
import dslab.transfer.TransferListener;
import dslab.util.Config;

public class MailboxServer implements IMailboxServer, Runnable {
    //ToDo add comments and sort this mess !
    private String componentId;
    private Config config;
    private Shell shell;
    private ServerSocket serverSocketDMTP;
    private ServerSocket serverSocketDMAP;
    public List<Message> messages = new ArrayList<>();
    public int messageCounter = 0;
    public Config usersConfig;
    private DmapListener dmapListener;
    private DmtpListener dmtpListener;

    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public MailboxServer(String componentId, Config config, InputStream in, PrintStream out) {
        // TODO
        this.config = config;
        shell = new Shell(in, out);
        shell.register(this);
        shell.setPrompt(componentId + "> ");
        usersConfig = new Config(config.getString("users.config"));
        this.componentId = componentId;

    }

    public String getComponentId(){
        return this.componentId;
    }

    @Override
    public void run() {
        // register mailbox server
        try {
            Registry registry = LocateRegistry.getRegistry(config.getString("registry.host"), config.getInt("registry.port"));
            INameserverRemote rootnameServer = (INameserverRemote) registry.lookup(config.getString("root_id"));
            rootnameServer.registerMailboxServer(config.getString("domain"),config.getString("registry.host")+":"+config.getInt("dmtp.tcp.port"));
        } catch (RemoteException e) {
            e.printStackTrace(); //ToDo
        } catch (NotBoundException e) {
            e.printStackTrace(); //ToDo
        } catch (InvalidDomainException e) {
            e.printStackTrace(); //ToDo
        } catch (AlreadyRegisteredException e) {
            e.printStackTrace(); //ToDo
        }

        // start dmap server
        try {
            serverSocketDMAP = new ServerSocket(config.getInt("dmap.tcp.port"));
            // handle incoming connections from client in a separate thread
            dmapListener = new DmapListener(serverSocketDMAP, this,config.getString("domain"),config.getString("users.config"));
            dmapListener.start();
        } catch (IOException e) {
            throw new UncheckedIOException("Error while creating server socket", e);
        }

        // start dmtp server
        try {
            serverSocketDMTP = new ServerSocket(config.getInt("dmtp.tcp.port"));
            // handle incoming connections from client in a separate thread
            dmtpListener = new DmtpListener(serverSocketDMTP, this,config.getString("domain"),config.getString("users.config"));
            dmtpListener.start();
        } catch (IOException e) {
            throw new UncheckedIOException("Error while creating server socket", e);
        }

        /*
         * Finally, make the Shell process the commands read from the
         * InputStream by invoking Shell.run(). Note that Shell implements the
         * Runnable interface, so you could theoretically run it in a new thread.
         * However, it is typically desirable to have one process blocking the main
         * thread. Reading from System.in (which is what the Shell does) is a good
         * candidate for this.
         */
        shell.run();

        // shutdown damp and dmtp
        dmapListener.shutdown();
        dmtpListener.shutdown();

        if(!serverSocketDMTP.isClosed()) {
            try {
                serverSocketDMTP.close();
            } catch (IOException e) {
                // cant handle this anyways
            }
        }

        if(!serverSocketDMAP.isClosed()) {
            try {
                serverSocketDMAP.close();
            } catch (IOException e) {
                // cant handle this anyways
            }
        }

        /*
         * The run method blocks until the read loop exits. To exit the loop
         * programmatically, a Command method may throw a StopShellException, which is
         * caught inside the Shell run method, causing the loop to break gracefully.
         */
        System.out.println("Exiting the shell, bye!");

    }

    @Command
    @Override
    public void shutdown() {
        // stop the Shell from reading from System.in by throwing a StopShellException
        throw new StopShellException();
    }

    public static void main(String[] args) throws Exception {
        IMailboxServer server = ComponentFactory.createMailboxServer(args[0], System.in, System.out);
        server.run();
    }
}
