package dslab.monitoring;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.util.Config;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.util.HashMap;

public class MonitoringServer implements IMonitoringServer {

    Shell shell;
    Config config;
    private DatagramSocket datagramSocket;
    HashMap<String,Integer> mapAddresses = new HashMap<>(32);
    HashMap<String,Integer> mapServers = new HashMap<>(32);
    private UdpListener udpListener;

    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public MonitoringServer(String componentId, Config config, InputStream in, PrintStream out) {
        // TODO
        this.config = config;

        shell = new Shell(in, out);
        shell.register(this);
        shell.setPrompt(componentId + "> ");
    }

    @Override
    public void run() {
        // TODO

        try {
            // constructs a datagram socket and binds it to the specified port
            datagramSocket = new DatagramSocket(config.getInt("udp.port"));

            // create a new thread to listen for incoming packets
            udpListener = new UdpListener(datagramSocket,mapAddresses,mapServers);
            udpListener.start();
        } catch (IOException e) {
            throw new RuntimeException("Cannot listen on UDP port.", e);
        }


        /*
         * Finally, make the Shell process the commands read from the
         * InputStream by invoking Shell.run(). Note that Shell implements the
         * Runnable interface, so you could theoretically run it in a new thread.
         * However, it is typically desirable to have one process blocking the main
         * thread. Reading from System.in (which is what the Shell does) is a good
         * candidate for this.
         */
        shell.run();

        // TODO
        udpListener.shutdown();

        /*
         * The run method blocks until the read loop exits. To exit the loop
         * programmatically, a Command method may throw a StopShellException, which is
         * caught inside the Shell run method, causing the loop to break gracefully.
         */
        System.out.println("Exiting the shell, bye!");
    }

    @Command
    @Override
    public void addresses() {
        for (String k:mapAddresses.keySet()) {
            shell.out().println(k + " " +mapAddresses.get(k));
        }
    }

    @Command
    @Override
    public void servers() {
        for (String k:mapServers.keySet()) {
            shell.out().println(k + " " +mapServers.get(k));
        }
    }

    @Command
    @Override
    public void shutdown() {
        // stop the Shell from reading from System.in by throwing a StopShellException
        throw new StopShellException();
    }

    public static void main(String[] args) throws Exception {
        IMonitoringServer server = ComponentFactory.createMonitoringServer(args[0], System.in, System.out);
        server.run();
    }

}
