package dslab.monitoring;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class UdpListener extends Thread {
    private DatagramSocket datagramSocket;          // socket for the udp connection
    private HashMap<String, Integer> mapAddresses;  // map that contains the addresses and counters
    private HashMap<String, Integer> mapServers;    // map that contains the servers and counters

    // flag to stop the server after shutdown command was inserted to CLI
    private final AtomicBoolean running = new AtomicBoolean(false);

    /**
     * create a new udp listener
     * @param datagramSocket
     * @param mapAddresses
     * @param mapServers
     */
    public UdpListener(DatagramSocket datagramSocket, HashMap<String, Integer> mapAddresses, HashMap<String, Integer> mapServers) {
        this.datagramSocket = datagramSocket;
        this.mapAddresses = mapAddresses;
        this.mapServers = mapServers;
    }

    /**
     * contains an endless loop that can be stopped throw the shutdown method
     */
    public void run() {
        byte[] buffer;
        DatagramPacket packet;
        running.set(true);
        try {
            while (running.get()) {
                buffer = new byte[1024];
                // create a datagram packet of specified length (buffer.length)
                /*
                 * Keep in mind that, in UDP, packet delivery is not guaranteed,
                 * and the order of the delivery/processing is also not guaranteed.
                 */
                packet = new DatagramPacket(buffer, buffer.length);

                // wait for incoming packets from client
                datagramSocket.receive(packet);
                // get the data from the packet
                String request = new String(packet.getData(), packet.getOffset(),packet.getLength());

                // get the address of the sender (client) from the received packet
                InetAddress address = packet.getAddress();
                // get the port of the sender from the received packet
                int port = packet.getPort();

                String response = "ok";

                String[] parts = request.split(" ");
                if (parts.length == 2) {
                    String mail = parts[1];
                    String server = parts[0];
                    if (mapServers.containsKey(server)) {
                        int current = mapServers.get(server);
                        current++;
                        mapServers.replace(server, current);
                    } else {
                        mapServers.put(server, 1);
                    }
                    if(mapAddresses.containsKey(mail)){
                        int next = mapAddresses.get(mail)+1;
                        mapAddresses.replace(mail, next);
                    } else {
                        mapAddresses.put(mail,1);
                    }
                } else {
                    response = "error";
                }

                buffer = response.getBytes();

                packet = new DatagramPacket(buffer, buffer.length, address, port);
                datagramSocket.send(packet);
            }
        } catch (SocketException e) {
            // when the socket is closed, the send or receive methods of the DatagramSocket will throw a SocketException
            System.out.println("SocketException while waiting for/handling packets: " + e.getMessage());
        } catch (IOException e) {
            // other exceptions should be handled correctly in your implementation
            throw new UncheckedIOException(e);
        } finally {
            if (datagramSocket != null && !datagramSocket.isClosed()) {
                datagramSocket.close();
            }
        }
    }

    /**
     * simple method to shutdown the server gently
     */
    public void shutdown(){
        running.set(false);
        if (datagramSocket != null && !datagramSocket.isClosed()) {
            datagramSocket.close();
        }
    }

}
