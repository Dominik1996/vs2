package dslab.client;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Scanner;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.util.Config;
import dslab.util.Keys;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

public class MessageClient implements IMessageClient, Runnable {

    /**
     * ToDo:
     * Add Comments
     * Describe variables
     * fill description for methods
     */

    private Config config;
    private InputStream inputStream;
    private PrintStream outputStream;

    private PrintWriter newConn_writer = null;
    private BufferedReader newConn_listener = null;
    private Socket mailboxSocket;
    private int mailboxPort;
    private String mailboxHost;
    private PublicKey publicServerKey;

    boolean useEncryption = false;
    byte[] msg_iv = null;
    SecretKeySpec key = null;

    SecureRandom secureRandom = new SecureRandom();
    boolean alive = true;

    private Shell shell;

    /**
     * Creates a new client instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public MessageClient(String componentId, Config config, InputStream in, PrintStream out) {
        this.config = config;
        this.inputStream = in;
        this.outputStream = out;

        this.mailboxPort = config.getInt("mailbox.port");
        this.mailboxHost = config.getString("mailbox.host");

        Security.addProvider(new BouncyCastleProvider());

        //ToDo what does this code
        try{
            this.mailboxSocket = new Socket(mailboxHost,mailboxPort);
            newConn_writer = new PrintWriter(mailboxSocket.getOutputStream(), true);
            newConn_listener = new BufferedReader(new InputStreamReader(mailboxSocket.getInputStream()));
            System.out.println(newConn_listener.readLine());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        shell = new Shell(in, out);
        shell.register(this);
        shell.setPrompt(componentId + "> ");

    }

    /**
     *
     */
    @Override
    public void run() {

        try {
            startingSec();
        } catch (Exception e) {
            e.printStackTrace();
            //todo
        }

        /**
         * todo
         */
        shell.run();

        System.out.println("Exiting the client, bye!");

    }

    /**
     *
     * @param componentId
     * @return
     * @throws IOException
     */
    PublicKey getPublicKey(String componentId) throws IOException {
        File keyFile = new File( System.getProperty("user.dir")+"/keys/client/"+componentId+".pub");
        return Keys.readPublicKey(keyFile);
    }

    /**
     *
     * @param message
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     */
    private String decodeCypher(String message) throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidAlgorithmParameterException
    {
        if(useEncryption) return new String(cryptAES(Base64.decode(message), false));
        else return message;
    }

    /**
     *
     * @param size
     * @return
     */
    private byte[] getRandomNr(int size)
    {
        // generates a 32 byte secure random number
        final byte[] arr = new byte[size];
        secureRandom.nextBytes(arr);
        return arr;
    }

    /**
     *
     * @return
     * @throws NoSuchAlgorithmException
     */
    private byte[] getKey() throws NoSuchAlgorithmException
    {
        // Generate secret key (AES cipher)
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(256); //128 is outdates and NOT secure
        byte[] secretKeyBytes = keyGenerator.generateKey().getEncoded();
        return secretKeyBytes;
    }

    /**
     *
     * @param data
     * @return
     */
    private String byteToS(byte[] data)
    {
        return new String(Base64.encode(data), StandardCharsets.UTF_8);
    }

    /**
     *
     * @param data
     * @return
     */
    private byte[] decodeBytes(String data)
    {
        return Base64.decode(data.getBytes());
    }

    /**
     *
     * @throws Exception
     */
    public void startingSec() throws Exception
    {
        String request = "";
        write("startsecure");

        //todo debug this code ?
        if ((request = newConn_listener.readLine()) != null) {

            String componenId = request.split(" ")[1];
            if(componenId.contains(".")) componenId = componenId.split(".")[0];
            PublicKey publicKey = getPublicKey(componenId);

            //CHALLANGE
            byte[] msg_challange = getRandomNr(32);
            msg_iv = getRandomNr(16);
            byte[] msg_key = getKey();
            key = new SecretKeySpec(msg_key, "AES");

            String step3 = "ok " + byteToS(msg_challange) + " " + byteToS(msg_key) + " " + byteToS(msg_iv);
            write(byteToS(applyRsa(step3, publicKey)));

            if ((request = newConn_listener.readLine()) != null) {
                byte[] message = decodeBytes(request);
                String retMsg = new String(cryptAES(message, false));
                String retChallange = retMsg.split(" ")[1];

                shell.out().println("My challange: " + byteToS(msg_challange));
                shell.out().println("Server response: " + retChallange);
                if (byteToS(msg_challange).equals(retChallange)) {
                    useEncryption = true;
                    write("ok");
                } else write("ERROR");
            } else {
                System.out.println("Error in agreeing upon a cipher");
            }
        }

        //login test ToDo
        write("login "+config.getString("mailbox.user") + " "+ config.getString("mailbox.password"));
        String req = newConn_listener.readLine();
        byte[] m = decodeBytes(request);
        String retMsg = new String(cryptAES(m, false));
        shell.out().println("response:" +retMsg);
    }

    /**
     *
     * @param data
     * @param publicKey
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    private byte[] applyRsa(String data, PublicKey publicKey) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException
    {
        // make sure to use the right ALGORITHM for what you want to do (see text)
        Cipher rsaCipher = Cipher.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding", "BC");
        // MODE is the encryption/decryption mode
        // KEY is either a private, public or secret key
        // IV is an init vector, needed for AES
        rsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return rsaCipher.doFinal(data.getBytes());
    }

    /**
     *
     * @param data
     * @param encrypt
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    private byte[] cryptAES(byte[] data, boolean encrypt) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException
    {
        // make sure to use the right ALGORITHM for what you want to do (see text)
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        // MODE is the encryption/decryption mode
        // KEY is either a private, public or secret key
        // IV is an init vector, needed for AES
        cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, key, new IvParameterSpec(msg_iv));
        return cipher.doFinal(data);
    }

    /**
     *
     * @param input
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws InvalidAlgorithmParameterException
     */
    private void write(String input) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException
    {
        String encoded = "";
        if(useEncryption) encoded = byteToS(cryptAES(input.getBytes(), true));
        if(useEncryption) newConn_writer.println(encoded);
        else newConn_writer.println(input);
        newConn_writer.flush();
        //ToDo no System.out !
        System.out.println("client: " + input);
        System.out.println("client_encoded: " + encoded);
    }


    /**
     *
     */
    @Command
    @Override
    public void inbox() {

    }

    /**
     *
     * @param id the mail id
     */
    @Command
    @Override
    public void delete(String id) {

    }

    /**
     *
     * @param id the message id
     */
    @Command
    @Override
    public void verify(String id) {

    }

    /**
     *
     * @param to comma separated list of recipients
     * @param subject the message subject
     * @param data the message data
     */
    @Command
    @Override
    public void msg(String to, String subject, String data) {
        String cmd = "";
        

    }

    /**
     *
     */
    @Command
    @Override
    public void shutdown() {
        alive = false;
    }

    public static void main(String[] args) throws Exception {
        IMessageClient client = ComponentFactory.createMessageClient(args[0], System.in, System.out);
        client.run();
    }
}
