package dslab.nameserver;

import java.io.InputStream;
import java.io.PrintStream;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.util.Config;

public class Nameserver implements INameserver, INameserverRemote {
    //!! ToDo add logging / basic output, check code complexity

    Config config;                      // hold current config
    Shell shell;                        // used for different output and input over the console

    Registry registry;                  // registry
    INameserverRemote root;             //
    INameserverRemote nameserver;       //

    ConcurrentMap<String, INameserverRemote> nameservers;      // holds the nameservers in it
    ConcurrentMap<String, String> mailservers;                 // holds the mailservers in it

    private boolean isRootNameserver;   // defines if this nameserver object is the root or if it is not
    private boolean isRunning = true;   // when this is set to false, stop all threads and shutdown



    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public Nameserver(String componentId, Config config, InputStream in, PrintStream out) {
        this.config = config;

        shell = new Shell(in, out);
        shell.register(this);
        shell.setPrompt(componentId + "> ");

        nameservers = new ConcurrentHashMap<>();
        mailservers = new ConcurrentHashMap<>();
    }

    @Override
    public void run() {
        isRunning = true;
        if(!this.config.containsKey("domain")){
            //out.println(LocalDateTime.now() + " : register root nameserver");

            // we have detected that we have a root nameserver now
            // so create a new registry object instead of only binding it
            isRootNameserver = true;
            try{
                // create registry object
                registry = LocateRegistry.createRegistry(config.getInt("registry.port"));

                // make object available for others
                nameserver = (INameserverRemote) UnicastRemoteObject.exportObject( this ,0);
                try{
                    // only root binds remote object to registry
                    registry.bind(config.getString("root_id"), nameserver);
                }catch (RemoteException e){
                    shell.out().println(getCurrentTimeStamp()+"error while starting registry for root nameserver"+e.getMessage());
                } catch (AlreadyBoundException e){
                    shell.out().println(getCurrentTimeStamp()+"error while starting registry for root nameserver"+e.getMessage());
                }

            }catch (RemoteException e){
                e.printStackTrace();
            }
        }else {
            isRootNameserver = false;

            // this object is not the root nameserver - so only hold the registry as inmemory reference
            try {
                registry = LocateRegistry.getRegistry(config.getString("registry.host"), config.getInt("registry.port"));
                nameserver = (INameserverRemote) UnicastRemoteObject.exportObject( this ,0);
                root = (INameserverRemote) registry.lookup(config.getString("root_id"));
                root.registerNameserver(config.getString("domain"),nameserver );
            }catch (RemoteException e){
                shell.out().println(getCurrentTimeStamp()+"error while starting registry for non root nameserver"+e.getMessage());
            }catch (NotBoundException e){
                shell.out().println(getCurrentTimeStamp()+"error while starting registry for non root nameserver"+e.getMessage());
            }catch (AlreadyRegisteredException e){
                shell.out().println(getCurrentTimeStamp()+"error while starting registry for non root nameserver"+e.getMessage());
            }catch (InvalidDomainException e){
                shell.out().println(getCurrentTimeStamp()+"error while starting registry for non root nameserver"+e.getMessage());
            }
        }

        /*
         * Finally, make the Shell process the commands read from the
         * InputStream by invoking Shell.run(). Note that Shell implements the
         * Runnable interface, so you could theoretically run it in a new thread.
         * However, it is typically desirable to have one process blocking the main
         * thread. Reading from System.in (which is what the Shell does) is a good
         * candidate for this.
         */
        shell.run();
    }

    @Command
    @Override
    public void nameservers() {
        // generate a list of all nameservers (which are keys here)
        List<String> nameserversSorted = new LinkedList<>();
        for(String key:nameservers.keySet()){
            nameserversSorted.add(key);
        }

        // sort them
        nameserversSorted.sort(Comparator.naturalOrder());

        // and output them
        int count = 1;
        for(String key : nameserversSorted){
            shell.out().println(count + ". " + key);
            count++;
        }
    }

    @Command
    @Override
    public void addresses() {
        // generate a list of all addresses (which are keys here)
        List<String> mailserverDomainsSorted = new LinkedList<>();
        for(String key:mailservers.keySet()){
            mailserverDomainsSorted.add(key);
        }

        // sort themn
        mailserverDomainsSorted.sort(Comparator.naturalOrder());

        // and output them
        int count = 1;
        for(String key:mailserverDomainsSorted){
            shell.out().println(count + ". " + key + " " + mailservers.get(key));
            count++;
        }
    }

    @Command
    @Override
    public void shutdown() {
        isRunning = false;

        shell.out().println(getCurrentTimeStamp()+"shutting down server");
        if(!isRootNameserver){
            // if its no root nameserver only unexport the remote object
            try{
                registry.unbind(config.getString("root_id"));
                UnicastRemoteObject.unexportObject(this,  false);
            }catch (NoSuchObjectException e){
                shell.out().println(getCurrentTimeStamp()+"error while shutting down server"+e.getMessage());
            }catch (NotBoundException e){
                shell.out().println(getCurrentTimeStamp()+"error while shutting down server"+e.getMessage());
            }catch (RemoteException e){
                shell.out().println(getCurrentTimeStamp()+"error while shutting down server"+e.getMessage());
            }
        }else {
            // in the case of a root nameserver unregister unexport and close by also using the registry to invoke
            try{
                UnicastRemoteObject.unexportObject(this,  true);

                // this is needed to ensure, that the application shuts down
                UnicastRemoteObject.unexportObject(registry,  false);
            }catch (NoSuchObjectException e){
                shell.out().println(getCurrentTimeStamp()+"error while shutting down server"+e.getMessage());
            }
        }
    }

    public static void main(String[] args) throws Exception {
        INameserver component = ComponentFactory.createNameserver(args[0], System.in, System.out);
        component.run();
    }

    @Override
    public void registerNameserver(String domain, INameserverRemote nameserver) throws RemoteException, AlreadyRegisteredException, InvalidDomainException {
        shell.out().println(getCurrentTimeStamp()+" try to register nameserver "+domain);

        String[] domaincheckup = domain.split("\\.");

        if(this.nameservers.containsKey(domaincheckup[domaincheckup.length-1])){
            if(domaincheckup.length == 1) throw new AlreadyRegisteredException("already registered");

            String newDomain = getNewDomain(domaincheckup);

            nameservers.get(domaincheckup[domaincheckup.length-1]).registerNameserver(newDomain, nameserver);

        }else {
            if(domaincheckup.length > 1) throw new InvalidDomainException("invalid domain");
            nameservers.put(domain,nameserver);
            shell.out().println(getCurrentTimeStamp()+" registered nameserver "+domain);
        }
    }

    @Override
    public void registerMailboxServer(String domain, String address) throws RemoteException, AlreadyRegisteredException, InvalidDomainException {
        shell.out().println(getCurrentTimeStamp()+" try to register mailboxserver "+domain);

        String[] domaincheckup = domain.split("\\.");
        if(this.nameservers.containsKey(domaincheckup[domaincheckup.length-1])){
            if(domaincheckup.length == 1 && mailservers.containsKey(domaincheckup)) throw new AlreadyRegisteredException("alreagy registered");

            String newDomain = "";
            for (int i = 0; i < domaincheckup.length - 1 ; i++) {
                if (i == 0) {
                    newDomain += domaincheckup[i];
                } else {
                    newDomain += "." + domaincheckup[i];
                }
            }
            nameservers.get(domaincheckup[domaincheckup.length-1]).registerMailboxServer(newDomain, address);

        }else {
            if(domaincheckup.length > 1) throw new InvalidDomainException("invalid domain");
            this.mailservers.put(domain,address);
            shell.out().println(getCurrentTimeStamp()+" registered mailboxserver "+domain);
        }
    }

    @Override
    public INameserverRemote getNameserver(String zone) throws RemoteException {
        shell.out().println(getCurrentTimeStamp()+": lookup "+ zone);
        if(!isRunning)
            throw new RemoteException("server is not running");

        if (nameservers.containsKey(zone))
            return nameservers.get(zone);
        else return null;
    }

    @Override
    public String lookup(String username) throws RemoteException {
        shell.out().println(getCurrentTimeStamp()+": lookup "+ username);

        if(!isRunning)
            throw new RemoteException("server is not running");

        if (mailservers.containsKey(username))
            return mailservers.get(username);
        else return null;
    }

    private String getNewDomain(String[] splittedDomain){
        String newDomain = "";
        for (int i = 0; i < splittedDomain.length - 1 ; i++) {
            if (i == 0) {
                newDomain += splittedDomain[i];
            } else {
                newDomain += "." + splittedDomain[i];
            }
        }
        return newDomain;
    }

    private String getCurrentTimeStamp() {
        return new SimpleDateFormat("HH:mm:ss").format(new Date());
    }
}
