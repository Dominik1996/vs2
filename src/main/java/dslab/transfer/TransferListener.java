package dslab.transfer;

import dslab.mailbox.DmapHandler;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class TransferListener extends Thread {
    private ServerSocket serverSocket;  // socket to accept new incoming connections
    private TransferServer parent;      // parent server (for access to the messages)
    private ThreadPoolExecutor executor;// contains all running threads

    // flag to stop the server after shutdown command was inserted to CLI
    private final AtomicBoolean running = new AtomicBoolean(false);

    /**
     * create a new transfer listener
     * @param serverSocket socket to accept new connections
     * @param parent parent to share resources
     */
    public TransferListener(ServerSocket serverSocket, TransferServer parent) {
        this.serverSocket = serverSocket;
        this.parent = parent;

        executor = new ThreadPoolExecutor(0,Integer.MAX_VALUE,2L, TimeUnit.MINUTES, new SynchronousQueue<Runnable>());
    }

    /**
     * endless run loop, should only be stopped by the shutdown method
     */
    public void run() {
        running.set(true);
        while (running.get()) {
            Socket socket;
            try {
                // wait for Client to connect
                socket = serverSocket.accept();

                TransferDmtpServer t = new TransferDmtpServer(parent,socket);
                executor.execute(t);
            } catch (SocketException e) {
                // when the socket is closed, the I/O methods of the Socket will throw a SocketException
                // almost all SocketException cases indicate that the socket was closed
                System.out.println("SocketException while handling socket: " + e.getMessage());
                break;
            } catch (IOException e) {
                // you should properly handle all other exceptions
                throw new UncheckedIOException(e);
            } finally {

            }

        }
    }

    /**
     * simple method to shutdown the server gently
     */
    public void shutdown(){
        running.set(false);
        for(Runnable t :executor.getQueue()){
            DmapHandler d = (DmapHandler) t;
            d.shutdown();
        }
    }
}
