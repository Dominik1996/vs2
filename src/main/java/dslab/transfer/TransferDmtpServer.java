package dslab.transfer;

import dslab.mailbox.Message;
import dslab.util.DmtpServer;

import java.net.Socket;
import java.util.List;

public class TransferDmtpServer extends DmtpServer {
    TransferServer server;

    public TransferDmtpServer(TransferServer parent, Socket clientSocket) {
        super(parent, clientSocket);
        this.server = parent;
    }

    @Override
    public boolean handleMessageComplete(String from, List<String> to, String subject, String data) {
        boolean check = super.handleMessageComplete(from, to, subject, data);
        if(!check)
            return check;

        for (String address:to) {
            try {
                server.Messages.put(new Message(-1,subject,data,from,address));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return true;
    }
}
