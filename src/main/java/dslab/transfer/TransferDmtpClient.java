package dslab.transfer;

import dslab.mailbox.MailboxDmtpServer;
import dslab.mailbox.Message;
import dslab.nameserver.INameserverRemote;
import dslab.util.Config;

import javax.xml.stream.FactoryConfigurationError;
import java.io.*;
import java.net.*;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.concurrent.atomic.AtomicBoolean;

public class TransferDmtpClient extends Thread {

    //ToDo: add comments here
    TransferServer server;
    Config monitoringConfig;
    Registry registry;
    INameserverRemote rootNameServer;

    // flag to stop the server after shutdown command was inserted to CLI
    private final AtomicBoolean running = new AtomicBoolean(false);

    /**
     * create a new transfer dmtp client
     * @param server
     */
    public TransferDmtpClient(TransferServer server, Config monitoringConfig, Registry registry, INameserverRemote rootNameServer) {
        this.server = server;
        this.monitoringConfig = monitoringConfig;
        this.registry = registry;
        this.rootNameServer = rootNameServer;
    }

    /**
     * contains an endless loop which tries to send messages (until shutdown is triggerd)
     */
    public void run() {
        running.set(true);
        while (running.get()) {
            try {
                Message m = server.Messages.take();
                String receiver = m.getTo();
                String domain = receiver.split("@")[receiver.split("@").length - 1];
                boolean successful = false;
                boolean error_send = false;
                if(isValidDomain(domain)){
                    if(!sendMessage(m, domain)){
                        Message errorMessage = new Message(0,
                                "error delivery failure",
                                "message delivery failed for message: "+m.getSubject() + " ; too: "+m.getTo(),
                                "mailer@"+server.getServerIp(), m.getFrom());
                        String errorReceiver = errorMessage.getTo();
                        String errorDomain = errorReceiver.split("@")[errorReceiver.split("@").length - 1];
                        sendMessage(errorMessage,errorDomain);
                        error_send = true;
                    }else {
                        successful = true;
                    }
                }
                if(!successful && !error_send){
                    Message errorMessage = new Message(0,
                            "error delivery failure",
                            "message delivery failed for message: "+m.getSubject() + " ; too: "+m.getTo(),
                            "mailer@"+server.getServerIp(), m.getFrom());
                    String errorReceiver = errorMessage.getTo();
                    String errorDomain = errorReceiver.split("@")[errorReceiver.split("@").length - 1];
                    sendMessage(errorMessage,errorDomain);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * send a message to the given domain
     * @param m message to send
     * @param domain target domain
     * @return true if successful, false in any other case
     */
    private boolean sendMessage(Message m, String domain) {
        Socket socket = null;
        boolean successful = true;
        try {
            /*
             * create a new tcp socket at specified host and port - make sure
             * you specify them correctly in the client properties file(see
             * client1.properties and client2.properties)
             */
            String[] s = getAddressForDomain(domain).split(":"); // ToDo replace this!
            int p = Integer.parseInt(s[1]);
            socket = new Socket("127.0.0.1", p);
            // create a reader to retrieve messages send by the server
            BufferedReader serverReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // create a writer to send messages to the server
            PrintWriter serverWriter = new PrintWriter(socket.getOutputStream());

            serverWriter.println("begin");
            serverWriter.flush();
            if(!readMessage(serverReader,"ok")){
                successful = false;
            }

            serverWriter.println("to " + m.getTo());
            serverWriter.flush();
            if(!readMessage(serverReader,"ok")){
                successful = false;
            }

            serverWriter.println("from " + m.getFrom());
            serverWriter.flush();
            if(!readMessage(serverReader,"ok")){
                successful = false;
            }

            serverWriter.println("subject " + m.getSubject());
            serverWriter.flush();
            if(!readMessage(serverReader,"ok")){
                successful = false;
            }

            serverWriter.println("data " + m.getData());
            serverWriter.flush();
            if(!readMessage(serverReader,"ok")){
                successful = false;
            }

            serverWriter.println("send");
            serverWriter.flush();
            if(!readMessage(serverReader,"ok")){
                successful = false;
            }

            serverWriter.println("quit");
            serverWriter.flush();
            if(!readMessage(serverReader,"ok")){
                successful = false;
            }

            String udpMessage = ""+socket.getInetAddress().getHostAddress()+":"+p+ " " + m.getTo();
            if(sendMonitoringMessage(udpMessage)){

            }else {
                System.out.println("error while transmitting monitoring message");
            }

        } catch (UnknownHostException e) {
            System.out.println("Cannot connect to host: " + e.getMessage());
            successful = false;
        } catch (SocketException e) {
            // when the socket is closed, the I/O methods of the Socket will throw a SocketException
            // almost all SocketException cases indicate that the socket was closed
            System.out.println("SocketException while handling socket: " + e.getMessage());
            successful = false;
        } catch (IOException e) {
            // you should properly handle all other exceptions
            throw new UncheckedIOException(e);
        } catch (Exception e) {
            successful = false;
        } finally {
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // Ignored because we cannot handle it
                }
            }
        }
        return successful;
    }

    /**
     * send a monitoring message
     * @param message message to send
     * @return true if successful, false in any other case
     */
    private boolean sendMonitoringMessage(String message) {
        DatagramSocket socket = null;
        BufferedReader userInputReader = null;
        try {
            // open a new DatagramSocket
            socket = new DatagramSocket();
            socket.setSoTimeout(1000);
            byte[] buffer;
            DatagramPacket packet;

            if (message == null || message.equals("")) {
                return false;
            }

            // convert the input String to a byte[]
            buffer = message.getBytes();
            // create the datagram packet with all the necessary information
            // for sending the packet to the server
            packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName("127.0.0.1"),12398);

            // send request-packet to server
            socket.send(packet);

            System.out.println("Send package with content: " + message);

            buffer = new byte[1024];
            // create a fresh packet
            packet = new DatagramPacket(buffer, buffer.length);
            // wait for response-packet from server
            socket.receive(packet);

            String answer = new String(packet.getData());

            System.out.println("Received packet from Server: " + answer);

            if(answer.equals("error")){
                return false;
            }
            return true;
        } catch (UnknownHostException e) {
            System.out.println("Cannot connect to host: " + e.getMessage());
            return false;
        } catch (SocketException e) {
            System.out.println("SocketException: " + e.getMessage());
            return false;
        }catch (SocketTimeoutException e) {
            return false;
        }
        catch (IOException e) {
            throw new UncheckedIOException(e);
        } finally {
            if (socket != null && !socket.isClosed()) {
                socket.close();
            }
            if (userInputReader != null) {
                try {
                    userInputReader.close();
                } catch (IOException e) {
                    // Ignored because we cannot handle it
                }
            }
        }
    }

    /**
     * read a message with an expected result
     * @param reader reader to read the message
     * @param resultContains expected result
     * @return true if read message contains the expected result
     */
    private boolean readMessage(BufferedReader reader, String resultContains) {
        while (running.get()) {
            String answer;
            try {
                if (reader.ready() && (answer = reader.readLine()) != null) {
                    return answer.contains(resultContains);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * check if the given domain is valid / can be found in the registry
     * @param domain domain
     * @return true if domain can be found, false in any other case
     */
    private boolean isValidDomain(String domain){
        try{
            getAddressForDomain(domain);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param domain
     * @return
     * @throws Exception
     */
    private String getAddressForDomain(String domain) throws Exception{
        INameserverRemote currentNode = rootNameServer;
        String domainleft = domain;
        String[] split;

        try{
            while (running.get()){
                split = domainleft.split("\\.");
                currentNode = currentNode.getNameserver(split[split.length-1]);
                if(split.length <= 1)
                {
                    return currentNode.lookup(domainleft);
                }
                domainleft = domainleft.substring(0,domainleft.length()-split[split.length-1].length()-1);
            }
        } catch (RemoteException e)
        {

        }


        throw new Exception("could not find address for domain");
    }

    /**
     * simple method to shutdown the server gently
     */
    public void shutdown(){
        running.set(false);
    }
}
