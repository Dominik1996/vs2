package dslab.transfer;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.ComponentFactory;
import dslab.mailbox.Message;
import dslab.nameserver.AlreadyRegisteredException;
import dslab.nameserver.INameserverRemote;
import dslab.nameserver.InvalidDomainException;
import dslab.util.Config;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class TransferServer implements ITransferServer, Runnable {

    private Config config;                          // config to get important settings
    private Shell shell;                            // shell for console in and output
    private ServerSocket serverSocket;              // socket for new connections
    private TransferListener transferListener;      // listener threads for new connections
    private TransferDmtpClient transferDmtpClient;  // thread (consumer) that sends messages and monitoring output
    private Registry registry;                      // holds the rmi connection in it (remote reference to it)
    private INameserverRemote rootNameserver;       // root nameserver of the registry

    /**
     * used as transfer element of the producer consumer problem
     * fixed size & special methods for putting and getting elements
     */
    public BlockingQueue<Message> Messages = new ArrayBlockingQueue<>(64);

    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public TransferServer(String componentId, Config config, InputStream in, PrintStream out) {
        this.config = config;

        shell = new Shell(in, out);
        shell.register(this);
        shell.setPrompt(componentId + "> ");
    }

    /**
     * start threads
     */
    @Override
    public void run() {
        // retrieve remote reference
        try {
            Registry registry = LocateRegistry.getRegistry(config.getString("registry.host"), config.getInt("registry.port"));
            rootNameserver = (INameserverRemote) registry.lookup(config.getString("root_id"));
        } catch (RemoteException e) {
            e.printStackTrace(); //ToDo
        } catch (NotBoundException e) {
            e.printStackTrace(); //ToDo
        }

        // create a new consumer thread
        transferDmtpClient = new TransferDmtpClient(this,new Config("monitoring"),registry,rootNameserver);
        transferDmtpClient.start();

        // create and start a new TCP ServerSocket
        try {
            serverSocket = new ServerSocket(config.getInt("tcp.port"));
            // handle incoming connections from client in a separate thread
            transferListener = new TransferListener(serverSocket, this);
            transferListener.start();
        } catch (IOException e) {
            throw new UncheckedIOException("Error while creating server socket", e);
        }

        System.out.println("server socket listener running");

        /*
         * Finally, make the Shell process the commands read from the
         * InputStream by invoking Shell.run(). Note that Shell implements the
         * Runnable interface, so you could theoretically run it in a new thread.
         * However, it is typically desirable to have one process blocking the main
         * thread. Reading from System.in (which is what the Shell does) is a good
         * candidate for this.
         */
        shell.run();

        // gently shutdown listener (shuts also down any "child threads"
        transferListener.shutdown();

        // shutdown sender
        transferDmtpClient.shutdown();

        // ensure server socket gets closed
        if(!serverSocket.isClosed()) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                // cant handle this anyways
            }
        }

        /*
         * The run method blocks until the read loop exits. To exit the loop
         * programmatically, a Command method may throw a StopShellException, which is
         * caught inside the Shell run method, causing the loop to break gracefully.
         */
        System.out.println("Exiting the shell, bye!");
    }

    /**
     * return ip if running server
     * @return ip of server
     */
    public String getServerIp(){
        return serverSocket.getInetAddress().getHostAddress();
    }

    @Override
    @Command
    public void shutdown() {
        // stop the Shell from reading from System.in by throwing a StopShellException
        throw new StopShellException();
    }

    public static void main(String[] args) throws Exception {
        ITransferServer server = ComponentFactory.createTransferServer(args[0], System.in, System.out);
        server.run();
    }

}
