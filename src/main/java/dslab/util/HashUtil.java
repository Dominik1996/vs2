package dslab.util;


import org.bouncycastle.util.encoders.Base64;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {


    public static String encrypt (String from,String to, String subject, String data) throws NoSuchAlgorithmException, InvalidKeyException, IOException {

        String formatedString = String.join("\n", from.trim(), to.trim(), subject.trim(), data.trim());
        //System.out.println("FORMATEDSTRING: "+formatedString);
        return calculateHash(formatedString);
    }


    public static String calculateHash (String mail) throws NoSuchAlgorithmException, InvalidKeyException, IOException {

            //load the shared secret
            SecretKey secretKey = Keys.readSecretKey(new File ("./keys/hmac.key"));

            // Create Mac Instance
            //initialize it
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKey);
            //calculate the hash
            byte[] hash = mac.doFinal(mail.getBytes());
            byte[] base64Hash = Base64.encode(hash);

        return new String(base64Hash);
    }

    public static boolean verifyHash (String from, String to, String subject, String data, String hash) throws NoSuchAlgorithmException, InvalidKeyException, IOException {
        boolean isValid= false;
        String formatedString = String.join("\n", from.trim(), to.trim(), subject.trim(), data.trim());
        String calculatedHash = calculateHash(formatedString);

        return isValid = MessageDigest.isEqual(calculatedHash.getBytes(),hash.getBytes());

    }


}
