package dslab.util;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class test {
    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, IOException {

        String from= "arthur@earth.planet";
        String to= "trillian@earth.planet";
        String subject="Hello";
        String data= "Hello my friend";

        String hash= HashUtil.encrypt(from,to,subject,data);
        System.out.println(hash);

        System.out.println(HashUtil.verifyHash(from,to,subject,data,hash));
        System.out.println(HashUtil.verifyHash("bla@bla.at",to,subject,data,hash));
    }
}
