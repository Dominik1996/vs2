package dslab.util;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DmtpServer extends Thread{
    private Socket clientSocket;            // socket for the communication with the client
    private Object parent;                  // parent object (to synchronize with it if needed)
    private BufferedReader reader = null;   // input reader for the socket
    private PrintWriter writer = null;      // writer for responding to clients requests

    // flag to stop the server after shutdown command was inserted to CLI
    private final AtomicBoolean running = new AtomicBoolean(false);

    public DmtpServer(Object parent, Socket clientSocket){
        this.parent = parent;
        this.clientSocket = clientSocket;
    }

    /**
     * Method to run the complete message loop
     */
    public void run() {
        // main loop for complete communication
        try {
            // set running to true
            running.set(true);

            // prepare the input reader for the socket
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            // prepare the writer for responding to clients requests
            writer = new PrintWriter(clientSocket.getOutputStream());

            // send DMAP ok message
            writer.println("ok DMTP");
            writer.flush();

            // receive the begin message, if we cant receive it connection is stopped
            if(receiveBeginMessage()){
                System.out.println(Thread.currentThread().getName() + ": successful begin message");
            } else { return; }

            // run main message loop
            runMessageLoop();

            // if we are here, main loop has stopped, try to stop everything in finally and then thread is finished

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (clientSocket != null && !clientSocket.isClosed()) {
                try { clientSocket.close(); }
                catch (IOException e) {/* can't handle this */ }
            }
        }
    }

    /**
     * receives the begin message
     * @return true if received, false if not
     * @throws IOException
     */
    private boolean receiveBeginMessage() throws IOException {
        while (running.get()) {
            String request;
            if (reader.ready() && (request = reader.readLine()) != null)
            {
                if (!request.equals("begin")) {
                    writer.println("error protocol error");
                    writer.flush();
                    clientSocket.close();
                    return false;
                } else {
                    writer.println("ok");
                    writer.flush();
                    return true;
                }
            }
        }
        return false;
    }

    /***
     * run the main message loop
     * @throws IOException
     */
    private void runMessageLoop() throws IOException {
        String from = "";                       // save the sender of the message
        List<String> to = new ArrayList<>();    // save one or multiple receivers of the message
        String subject = "";                    // save the subject of the message
        String data = "";                       // save the content of the message
        String request = "";                    // temp save for the last input that was received

        while (running.get()){
            if (reader.ready() && (request = reader.readLine()) != null){
                System.out.println(Thread.currentThread().getName() + ": got request -->" + request);
                String[] parts = request.split("\\s");
                switch (parts[0]){
                    case "to":
                        if (parts.length < 2)
                            writer.println("error protocol error");
                        else {
                            to = new ArrayList<>();
                            boolean foundInvalid = false;
                            String invalidReceivers = "";
                            for (int i = 1; i < parts.length; i++){
                                if(checkReceiver(parts[i]))
                                    to.add(parts[i]);
                                else{
                                    foundInvalid = true;
                                    invalidReceivers += " " + parts[i];
                                }
                            }
                            if(foundInvalid){
                                to = new ArrayList<>();             // reset to list, because of invalid receivers
                                writer.println("error unknown receiver:"+invalidReceivers);
                                System.out.println("invalid receivers found"+invalidReceivers);
                            } else
                                writer.println("ok " + (to.size()));
                        }
                        break;
                    case "from":
                        if (parts.length != 2)
                            writer.println("error protocol error");
                        else {
                            if(checkSender(parts[1])){
                                writer.println("ok");
                                from = parts[1];
                            } else
                                writer.println("error invalid sender");
                        }
                        break;
                    case "subject":
                        if (parts.length < 2)
                            writer.println("error protocol error");
                        else {
                            subject = "";   // reset subject
                            for (int i = 1; i < parts.length; i++)
                                subject += parts[i] + " ";
                            writer.println("ok");
                        }
                        break;
                    case "data":
                        if (parts.length < 2)
                            writer.println("error protocol error");
                        else {
                            data = "";  // reset data
                            for (int i = 1; i < parts.length; i++)
                                data += parts[i] + " ";
                            writer.println("ok");
                        }
                        break;
                    case "send":
                        if(handleMessageComplete(from,to,subject,data))
                            writer.println("ok");
                        break;
                    case "quit":
                        writer.println("ok bye");
                        writer.flush();
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println(Thread.currentThread().getName() + ": closing socket now");
                        clientSocket.close();
                        return;
                    default:
                        writer.println("error protocol error");
                        writer.flush();
                        clientSocket.close();
                        return;
                }
                writer.flush();
            }
        }

    }

    /**
     * check the sender mail address with a simple regex
     * @param address
     * @return
     */
    private boolean checkSender(String address){
        Pattern regex = Pattern.compile("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", Pattern.CASE_INSENSITIVE);
        Matcher m = regex.matcher(address);
        return  m.matches();
    }

    /**
     * check the receiver mail address
     * @param address
     * @return
     */
    public boolean checkReceiver(String address){
        // sound kind of stupid but is needed to ensure
        // that this method can be rewritten later on
        return checkSender(address);
    }

    /**
     * handle the completed message
     * @param from
     * @param to
     * @param subject
     * @param data
     * @return
     */
    public boolean handleMessageComplete(String from, List<String> to, String subject, String data){
        if(to.size() == 0){
            writer.println("error invalid receiver(s)");
            return false; // size must be larger than zero !
        }

        if(from.equals("") || !checkSender(from)){
            writer.println("error invalid sender");
            return false; // from not valid
        }

        if(subject.equals("")){
            writer.println("error invalid subject");
            return false;
        }

        if(data.equals("")){
            writer.println("error invalid data");
            return false;
        }

        return true;
    }

    /**
     * simple method to shutdown the server gently
     */
    public void shutdown(){
        running.set(false);
    }
}
